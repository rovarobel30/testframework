package model;

import util.Annotation;
import util.ModelView;

import java.util.HashMap;

public class Emp {
    int idEmp;
    String empName;

    public Emp() {
    }

    public Emp(int idEmp, String empName) {
        this.idEmp = idEmp;
        this.empName = empName;
    }

    public int getIdEmp() {
        return idEmp;
    }

    public void setIdEmp(int idEmp) {
        this.idEmp = idEmp;
    }

    public String getEmpName() {
        return empName;
    }

    public void setEmpName(String empName) {
        this.empName = empName;
    }

    @Annotation(annotationMethod = "emp-list")
    public ModelView list(){
        ModelView modelView = new ModelView();
        modelView.setUrlJsp("emp-list.jsp");
        String[] list = {"Emp1","Emp2","Emp3","Emp4"};
        HashMap<String,Object> data = new HashMap<>();
        data.put("emp-list",list);
        modelView.setData(data);
        return modelView;
    }
}
