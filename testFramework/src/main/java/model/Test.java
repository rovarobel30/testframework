package model;

import util.Annotation;

public class Test {
    int idTest;
    String testName;

    public Test(int idTest, String testName) {
        this.idTest = idTest;
        this.testName = testName;
    }

    public int getIdTest() {
        return idTest;
    }

    public void setIdTest(int idTest) {
        this.idTest = idTest;
    }

    public String getTestName() {
        return testName;
    }

    public void setTestName(String testName) {
        this.testName = testName;
    }

    @Annotation(annotationMethod = "test-list")
    public String list(){
        return "SELECT * FROM Test";
    }
}
