package model;

import util.Annotation;
import util.ModelView;

import java.util.HashMap;

public class Dept {
    int idDept;
    String deptName;

    public Dept() {
    }

    public Dept(int idDept, String deptName) {
        this.idDept = idDept;
        this.deptName = deptName;
    }

    public int getIdDept() {
        return idDept;
    }

    public void setIdDept(int idDept) {
        this.idDept = idDept;
    }

    public String getDeptName() {
        return deptName;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

    @Annotation(annotationMethod = "dept-list")
    public ModelView list(){
        ModelView modelView = new ModelView();
        modelView.setUrlJsp("dept-list.jsp");
        String[] list = {"Dept1","Dept2","Dept3","Dept4"};
        HashMap<String,Object> data = new HashMap<>();
        data.put("dept-list",list);
        modelView.setData(data);
        return modelView;
    }

    @Annotation(annotationMethod = "dept-insert")
    public ModelView insert(){
        ModelView modelView = new ModelView();
        modelView.setUrlJsp("dept-insert.jsp");
        HashMap<String,Object> data = new HashMap<>();
        data.put("dept-insert",this.getDeptName());
        modelView.setData(data);

        return modelView;
    }
}
